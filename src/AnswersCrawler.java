import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.Scanner;

public class AnswersCrawler {

    public static void init() throws IOException{
        Scanner sc = new Scanner(System.in);
        boolean exitLoop = false;
        while(!exitLoop) {
            System.out.println("Options :\n1.) All Categories\n2.) Particular Sub-Category\n3.) Exit");
            switch (sc.nextInt()) {
                case 1:
                    Category root = new Category();
                    System.out.println("Checking for Category Hierarchy");
                    if(!root.isVisited()) {
                        root.setSubCategoryTree(0);
                        System.out.println("Category Tree Defined");
                    } else {
                        System.out.println("Category Tree Ready");
                    }
                    break;
                case 2:
                    System.out.println("Enter the SubCategory Name : ");
                    String subCat = sc.nextLine();
                    break;
                case 3:
                    exitLoop = true;
                    break;
                default:
                    System.out.println("Invalid option. Please enter again");
            }
        }
    }

    public static void main(String[] args) throws IOException {
        init();
    }
}
