import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.jsoup.Jsoup;
import org.jsoup.nodes.*;
import org.jsoup.select.Elements;
import org.neo4j.graphdb.*;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;

public class Category {
    static private final File DB_PATH = new File("/Users/aadit/Dropbox/AnswersCrawler/db");
    private static GraphDatabaseService graphdb;
    static private int idcounter = 0;
    private String name;
    private String url;
    private Node currentNode;
    private boolean isVisited;

    /* Enums for Relationships */
    private enum Relationships implements RelationshipType {
        SUBCATEGORY, BELONGS
    }

    Category(){
        graphdb = new GraphDatabaseFactory().newEmbeddedDatabase(DB_PATH);
        registerShutdownHook(graphdb);
        Label category = DynamicLabel.label("Category");
        Node rootNode = null;
        try (Transaction tx = graphdb.beginTx()) {
            rootNode = graphdb.findNode(category, "categoryName", "root");
            tx.success();
        } catch (MultipleFoundException e) {
            System.out.println("Multiple instances of root");
            e.printStackTrace();
        }
        if(rootNode == null) {
            this.name = "root";
            this.url = "http://www.answers.com/T";
            try (Transaction tx = graphdb.beginTx()) {
                this.currentNode = graphdb.createNode(category);
                this.currentNode.setProperty("categoryID", idcounter++);
                this.currentNode.setProperty("categoryName", this.name);
                this.currentNode.setProperty("categoryURL", this.url);
                this.isVisited = false;
                tx.success();
            }
        }
        else {
            this.currentNode = rootNode;
            this.isVisited = true;
        }
    }

    /*Category(String name) throws IOException {
        String url = "www.answers.com/T/"+name;
        url = url.replace(" ", "_");
        boolean status = Jsoup.connect(url).response().statusCode() == 404;
        if(!status) {
            Document category_doc = Jsoup.connect(url).get();
            if(!url.equals("http://www.answers.com/T"))
                this.addQuestions();
            for(int i=1; i<=level; i++) {
                System.out.print("\t");
                System.out.println("ADDED -- [" + category.select("span.category_name").first().text() + "] -- " + this.url);
            }
            try {
                //Politeness Delay
                TimeUnit.MILLISECONDS.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(currentCategory.getCategoryName().compareTo("Catch-All Questions") != 0 && !currentCategory.isVisited())
                currentCategory.setSubCategoryTree((level+1));
        }
    }*/

    private Category(Node parentNode, String name, String url) {
        this.name = name;
        this.url = url;
        Label category = DynamicLabel.label("Category");
        try (Transaction tx = graphdb.beginTx()) {
            Node searchNode = graphdb.findNode(category, "categoryName", name);
            if (searchNode == null) {
                this.currentNode = graphdb.createNode(category);
                this.currentNode.setProperty("categoryID", idcounter++);
                this.currentNode.setProperty("categoryName", name);
                this.currentNode.setProperty("categoryURL", url);
                this.isVisited = false;
            }
            else if(name.compareToIgnoreCase((String) searchNode.getProperty("categoryName"))==0) {
                this.currentNode = searchNode;
                this.isVisited = true;
                System.out.println("*** Parent : " + parentNode.getProperty("categoryName") + " ***");
                System.out.println("*** Current Node: " + searchNode.getProperty("categoryName") + " ***");
            }
            if(this.getRelationshipBetween(parentNode, this.currentNode) == null)
                parentNode.createRelationshipTo(this.currentNode, Relationships.SUBCATEGORY);
            tx.success();
        }
    }

    boolean isVisited() {
        return isVisited;
    }

    private String getCategoryName() {
        return this.name;
    }

    private Relationship getRelationshipBetween(Node n1, Node n2) {
        for (Relationship r : n1.getRelationships())
            if (r.getOtherNode(n1).equals(n2)) return r;
        return null;
    }

    public void setSpecificSubCategory(int level) throws IOException {

    }

    public void setSubCategoryTree(int level) throws IOException {
        Document main_doc = Jsoup.connect(this.url).timeout(10*1000).get();
        Elements categories = main_doc.select("ul.category_tree > li.category_item");
        if(categories.hasText()){
            for(Element category : categories){
                Category currentCategory = new Category(this.currentNode, category.select("span.category_name").first().text(), category.select("a").attr("href"));
                if(!this.url.equals("http://www.answers.com/T"))
                    this.addQuestions();
                for(int i=1; i<=level; i++) {
                    System.out.print("\t");
                    System.out.println("ADDED -- [" + category.select("span.category_name").first().text() + "] -- " + this.url);
                }
                try {
                    //Politeness Delay
                    TimeUnit.MILLISECONDS.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if(currentCategory.getCategoryName().compareTo("Catch-All Questions") != 0 && !currentCategory.isVisited())
                    currentCategory.setSubCategoryTree((level+1));
            }
        }
    }

    private void addQuestions() throws IOException {
        Document main_doc = Jsoup.connect(this.url).get();
        int last_page = -1;
        try {
            last_page = Integer.parseInt(main_doc.select("div.hgroup > div.pagination > a.last_page").text());
        } catch(Exception e) {
            System.out.println(this.url);
            e.printStackTrace();
        }
        int iterator = 1;
        do {
            processPage(this.url+"-"+iterator);
            iterator++;
        }while (iterator <= last_page);
    }

    private void processPage(String page_url) throws IOException{
        Document pageDoc = Jsoup.connect(page_url).get();
        Elements questions = pageDoc.select("div.question");
        if (!questions.isEmpty()) {
            for (Element question : questions) {
                String q_link = question.select("a").attr("href");
                processQuestion(q_link);
            }
        }
    }

    private void processQuestion(String q_link) throws IOException {
        Document qna_link = Jsoup.connect(q_link).get();
        Element q_subcat, title, popularity, answer, answer_provider, confidence_num;
        q_subcat = qna_link.select("div.category").first().select("a.category_name").last();
        title = qna_link.select("span.title_text").first();
        popularity = qna_link.select("span.people_text").first();
        answer = qna_link.select("div.answer_text").first();
        answer_provider = qna_link.select("div.user_info_text_node > a").first();
        try {
            confidence_num = qna_link.select("span.confidence_num").first();
        } catch(NullPointerException e){
            confidence_num = null;
        }
        Label question = DynamicLabel.label("Questions");
        try (Transaction tx = graphdb.beginTx()) {
            Node qna = graphdb.createNode(question);
            qna.setProperty("Question SubCategory", this.getCategoryName());
            qna.setProperty("Question", title.text());
            qna.setProperty("Popularity", popularity.text().substring(0,1));
            qna.setProperty("Answer", answer.text());
            qna.setProperty("AnswerProvider", answer_provider.text());
            if(confidence_num != null)
                qna.setProperty("Confidence Votes", confidence_num.text());
            else
                qna.setProperty("Confidence Votes", "0");
            qna.createRelationshipTo(this.currentNode, Relationships.BELONGS);
            tx.success();
        }
        System.out.println("\t"+this.getCategoryName()+" -- Added Question : "+title.text());
    }

    private static void registerShutdownHook( final GraphDatabaseService graphDb )
    {
        // Registers a shutdown hook for the Neo4j instance so that it
        // shuts down nicely when the VM exits (even if you "Ctrl-C" the
        // running application).
        Runtime.getRuntime().addShutdownHook( new Thread()
        {
            @Override
            public void run()
            {
                graphDb.shutdown();
            }
        } );
    }
}
